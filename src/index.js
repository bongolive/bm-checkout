var httpRequest;
var beemPage;
function removeAnimated(elem, price) {
  elem.target.removeAttribute("disabled");
  elem.target.innerHTML = "Pay " + price;
  elem.target.className = "beem-action";
  elem.target.addEventListener("click", clickEvent);
}

function closeButton(page) {
  page.firstElementChild
    .querySelector("button")
    .addEventListener("click", (e) => {
      if (e.path) {
        var k = e.path[2];
        k.removeChild(e.path[2].children[1]);
        page.removeChild(page.firstElementChild);
      } else if (e.target) {
        page.removeChild(e.target.parentElement.offsetParent.children[1]);
        page.removeChild(e.target.parentElement.offsetParent.children[0]);
      }
    });
}

function APICall(price, mobile, reference, transaction, token, elem) {
  console.log("APICall", { price, mobile, reference, transaction, token });
  beemPage = document.querySelector("#beem-page");
  if (window.XMLHttpRequest) {
    httpRequest = new XMLHttpRequest();
  } else if (window.ActiveXObject) {
    httpRequest = new ActiveXObject("Microsoft.XMLHTTP");
  }

  httpRequest.onreadystatechange = function() {
    if (httpRequest.readyState === XMLHttpRequest.DONE) {
      if (httpRequest.status === 200) {
        const res = JSON.parse(httpRequest.responseText);
        localStorage.setItem("key", res.token);
        removeAnimated(elem, price);
        beemPage.innerHTML =
          '<div class="beem-page__close"><button>&times; &nbsp;Close</button></div><iframe src="' +
          res.src +
          '"></iframe>';
        closeButton(beemPage);
      } else {
        const err = JSON.parse(httpRequest.responseText);
        console.log({ err });
        removeAnimated(elem, price);
        beemPage.innerHTML =
          '<div class="beem-page__close"><button>&times; &nbsp;Close</button></div>' +
          "<iframe src='" +
          err.src +
          "' style='height: 200px!important'></iframe>";
        closeButton(beemPage);
      }
    }
  };
  var url = "%process.env.APP_DOMAIN%/get-iframe?" + "price=" + price;
  if (mobile) url = url + "&mobile=" + mobile;
  if (reference) url = url + "&reference_number=" + reference;
  if (transaction) url = url + "&transaction_id=" + transaction;
  if (token) url = url + "&secure_token=" + token;
  httpRequest.open("GET", url);
  httpRequest.withCredentials = true;
  httpRequest.send();
}

function clickEvent(e) {
  var price = e.target.dataset.price;
  var mobile = e.target.dataset.mobile;
  var transaction = e.target.dataset.transaction;
  var reference = e.target.dataset.reference;
  var token = e.target.dataset.token;
  e.target.innerHTML = "<span class='lds-dual-ring'></span> Pay" + price;
  e.target.setAttribute("disabled", true);
  e.target.className += " disabled";
  APICall(price, mobile, reference, transaction, token, e);
}

function InitializeBeem() {
  var btns = document.querySelectorAll("#beem-button");
  btns.forEach(function(elem) {
    var price = elem.dataset.price;
    var mobile = elem.dataset.mobile;
    var reference = elem.dataset.reference;
    var transaction = elem.dataset.transaction;
    var token = elem.dataset.token;
    console.log({ token });
    // UUIDv4
    const regex = /^[0-9a-f]{8}-[0-9a-f]{4}-[0-5][0-9a-f]{3}-[089ab][0-9a-f]{3}-[0-9a-f]{12}$/i;

    const regexAmount = /^[\d]*$/;

    console.log({ transaction: regex.test(transaction) });
    if (regexAmount.test(price) && regex.test(transaction)) {
      if (mobile) {
        elem.innerHTML =
          "<button id='beem-action' class='beem-action' data-price='" +
          price +
          "' data-transaction='" +
          transaction +
          "' data-token='" +
          token +
          "' data-mobile='" +
          mobile +
          "' data-reference='" +
          reference +
          "'>Pay " +
          price +
          "</button>";
      } else {
        elem.innerHTML =
          "<button id='beem-action' class='beem-action' data-price='" +
          price +
          "' data-transaction='" +
          transaction +
          "' data-token='" +
          token +
          "' data-reference='" +
          reference +
          "'>Pay " +
          price +
          "</button>";
      }
    }
    var actionBtn = elem.querySelector("#beem-action");
    // if (reference) actionBtn.setAttribute('data-reference', reference)
    actionBtn.addEventListener("click", clickEvent);
  });
}

module.exports = InitializeBeem;
