# Installation
using npm

```
npm install beem-checkout
```
using yarn

```
yarn add beem-checkout
```

# usage

- Add the stylesheet to the main html page 

```html
  <link rel="stylesheet" href="https://checkout.beem.africa/dist/0.1_alpha/bpay.min.css" />
```

- Add the following html content to there respect position on your page

```html
 <div id="beem-button" data-price='' data-mobile='' data-token='' data-reference='' data-transaction=''></div>
 <div id="beem-page" class="beem-page"></div>
```

   * data-price - The price of the item, Decimals not allowed (<i>required</i>)
   * data-reference - Reference number of the item, Should be alphanumeric.The prefix pattern should be added when creating collections & checkout products. Example: SAMPLE-12345.
   * data-mobile - Clients mobile number with country code prefix e.g. 255701000000 (<i>optional</i>)
   * data-token - An identification token sent by the client when requesting validation on their side, this will be sent back in the callback as headers (<i>optional</i>)
   * data-transaction- TTransaction ID of the item, Should be UUIDv4. E.g. 96f9cc09-afa0-40cf-928a-d7e2b27b2408  (<i>required</i>)


- After adding those to there respective places just initialize the library

<!-- React example | [Demo](https://ij1lu.csb.app/) -->
React example

 ```javascript
import React, { useEffect } from "react";

import initializeBeem from "beem-checkout";

function App() {
  useEffect(() => {
    initializeBeem();
  }, []);
  return (
    <div>
      <div
        id="beem-button"
        data-price="2000"
        data-reference="SAMPLE-12345"
        data-transaction="96f9cc09-afa0-40cf-928a-d7e2b27b2408"
        data-mobile="255701000000"
        data-token="XYZ"
      ></div>
      <div id="beem-page" className="beem-page"></div>
    </div>
  );
}

export default App;
```

Library can also be used with angular.